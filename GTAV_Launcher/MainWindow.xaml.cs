﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using System.Threading;

namespace GTAV_Launcher
{
    
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private bool scripted = false;
        public bool Scripted
        {
            get { return scripted; }
            set
            {
                if (value)
                    this.Title += " [Script]";
                else
                    this.Title.Replace(" [Script]", "");
                scripted = value;
            }
        }

        public MainWindow()
        {
            try
            {
                InitializeComponent();
                
                Directory.SetCurrentDirectory(AppDomain.CurrentDomain.BaseDirectory);
                CheckOnline.IsChecked = Properties.Settings.Default.OfflineChecked;
                RemoveOnExit.IsChecked = Properties.Settings.Default.RemoveOnExit;
            }
            catch(Exception ex)
            {
                MessageBox.Show(string.Format("An unknown error has occured.\n{0}", ex.Message), "GTA Launcher has crashed... :(");
                this.Close();
            }
        }

        private string installationPath;
        private string modStorePath;
        private string binaryPath;
        private string commandlineFilePath;
        private bool isSteam;
        public string CurrentInstallationPath
        {
            get { return installationPath; }
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    modStorePath = System.IO.Path.Combine(value, "mod_store");
                    binaryPath = System.IO.Path.Combine(value, "GTAVLauncher.exe");
                    //isSteam = !File.Exists(System.IO.Path.Combine(value, "PlayGTAV.exe"));
                    commandlineFilePath = System.IO.Path.Combine(value, "commandline.txt");
                    sp_mods.IsEnabled = true;
                    sp_vanilla.IsEnabled = true;
                    gtaOnline.IsEnabled = true;
                    installationPath = value;

                    if (isSteam)
                        this.Title += " [S]";
                    else
                        this.Title += " [W]";
                }
                else
                {
                    MessageBox.Show("GTA is not detected as installed (Registry key is missing).\nTry Placing this program in the game directory.", "GTA is not installed", MessageBoxButton.OK, MessageBoxImage.Error);
                    sp_mods.IsEnabled = false;
                    sp_vanilla.IsEnabled = false;
                    gtaOnline.IsEnabled = false;
                }
            }
        }

        private string InstallPath(bool edit)
        {
            try
            {
                if (File.Exists(System.IO.Path.Combine(Environment.CurrentDirectory, "GTA5.exe")))
                {
                    isSteam = !File.Exists(System.IO.Path.Combine(Environment.CurrentDirectory, "PlayGTAV.exe"));
                    return Environment.CurrentDirectory;
                }

                string path = string.Empty;
                using (RegistryKey key = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node\rockstar games\Grand Theft Auto V", edit))
                {
                   
                    if (key != null)
                    {                        
                        path = key.GetValue("InstallFolder", "").ToString();
                    }

                    if (!string.IsNullOrWhiteSpace(path))
                    {
                        isSteam = false;
                        return path;
                    }

                }

                using (RegistryKey key = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\Steam App 271590", edit))
                {                    
                    if (key != null)
                    {
                        path = key.GetValue("InstallLocation", "").ToString();
                    }

                    if (!string.IsNullOrWhiteSpace(path))
                    {
                        isSteam = true;
                        return path;
                    }
                }

                return string.Empty;
            }
            catch
            {
                MessageBox.Show("There was an error reading the registry. You must run this from an account with administrative privileges!");
                return string.Empty;
            }
        }

        private void sp_vanilla_Click(object sender, RoutedEventArgs e)
        {
            string argument = string.Empty;
            if(CheckOnline.IsChecked??false)
                argument = "-scOfflineOnly";
            startGTA(false, argument);
        }

        private void sp_mods_Click(object sender, RoutedEventArgs e)
        {
            string argument = string.Empty;
            if (CheckOnline.IsChecked ?? false)
                argument = "-scOfflineOnly";
            startGTA(true, argument);
        }



        private void gtaOnline_Click(object sender, RoutedEventArgs e)
        {
            startGTA(false, "-StraightIntoFreemode");
        }

        private void startGTA(bool mods, string commandline)
        {
            if (mods)
            {
                if (!enableMods())
                    return;
            }
            else
                if (!disableMods())
                    return;

            start(commandline, mods);
        }

        private void start(string mode, bool mods)
        {
            try
            {
                if (!Scripted)
                {
                    File.WriteAllText(commandlineFilePath, mode);
                    if (!isSteam)
                        Process.Start(binaryPath);
                    else
                        Process.Start("steam://rungameid/271590");
                }

                if (mods)
                {
                    if (RemoveOnExit.IsChecked ?? false)
                    {
                        this.WindowState = System.Windows.WindowState.Minimized;
                        Process thisProc = Process.GetCurrentProcess();
                        thisProc.PriorityClass = ProcessPriorityClass.BelowNormal;

                        Thread thread = new Thread(() =>
                        {

                            Thread.Sleep(1 * 60 * 1000); //1 minute
                            Process[] gtav = Process.GetProcessesByName("gta5");
                            foreach (Process p in gtav)
                            {
                                while (!p.HasExited)
                                {
                                    Thread.Sleep(30000); //30seconds
                                }
                            }
                            File.WriteAllText(commandlineFilePath, "");
                            disableMods();

                        });
                        thread.IsBackground = true;
                        thread.Start();
                        thread.Join();
                    }                    
                }
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message,"Could not start GTAVLauncher.exe");                
            }
        }

        private string[] loads = { "scripthookv.dll", "dsound.dll", "dinput8.dll", "ScriptHookVDotNet.dll" };

        private bool moveFile(string fileLocation, string fileDestination, bool delete)
        {
            try
            {
                File.Delete(fileDestination);
                if (File.Exists(fileDestination))
                    throw new Exception("Destination File already exists.");
                File.Move(fileLocation, fileDestination);
                //File.Copy(fileLocation, fileDestination, true);
                if(delete)
                    File.Delete(fileLocation);
            }
            catch (Exception ex)
            {
                if (MessageBox.Show(string.Format("There was an error copying file {0}, do you want to continue?\n{1}", System.IO.Path.GetFileName(fileLocation), ex.Message), "File move failure", MessageBoxButton.YesNo, MessageBoxImage.Error) == MessageBoxResult.No)
                    return false;
            }
            return true;
        }
        private bool disableMods()
        {
            if (!Directory.Exists(modStorePath))
                Directory.CreateDirectory(modStorePath);

            

            foreach (string loader in loads)
            {
              
                string loaderLocation = System.IO.Path.Combine(CurrentInstallationPath,loader);
                if (!File.Exists(loaderLocation))
                    continue;
                string loaderCopylocation = System.IO.Path.Combine(modStorePath, loader);
                if (!moveFile(loaderLocation, loaderCopylocation,true))
                    return false;
            }

            foreach (string file in Directory.GetFiles(CurrentInstallationPath, "*.*", SearchOption.TopDirectoryOnly))
            {
                string filename = System.IO.Path.GetFileName(file).ToLower();
                if (filename.Contains(".asi"))
                {
                    if (!moveFile(file, System.IO.Path.Combine(modStorePath, filename), true))
                        return false;
                }
            }

            return true;
        }
        private bool enableMods()
        {
            if (Directory.Exists(modStorePath))
            {
                foreach (string file in Directory.GetFiles(modStorePath, "*.*", SearchOption.TopDirectoryOnly))
                {
                    string filename = System.IO.Path.GetFileName(file).ToLower();
                    if (!moveFile(file, System.IO.Path.Combine(CurrentInstallationPath, filename), false))
                        return false;
                }
            }
            return true;
        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            CurrentInstallationPath = InstallPath(false);
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.OfflineChecked = CheckOnline.IsChecked ?? false;
            Properties.Settings.Default.Save();
        }

        private void RemoveOnExit_Checked(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.RemoveOnExit = RemoveOnExit.IsChecked ?? false;
            Properties.Settings.Default.Save();
        }

        

     
    }
}
