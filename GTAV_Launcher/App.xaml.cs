﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;

namespace GTAV_Launcher
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            bool scripted = false;
            
            if (e.Args.Length > 0)            
                scripted = true;

            MainWindow mainWindow = new MainWindow();
            mainWindow.Scripted = scripted;
            mainWindow.Show();
        }
    }
}
