﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace HL_Launcher
{
    class GTAV
    {
        private static bool scripted = false;
        public static bool Scripted
        {
            get { return scripted; }
            set
            {
                scripted = value;
            }
        }
        private static string installationPath;
        private static string modStorePath;
        private static string binaryPath;
        private static bool isSteam;

        public static string CurrentInstallationPath
        {
            get { return installationPath; }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    modStorePath = System.IO.Path.Combine(value, "mod_store");
                    binaryPath = System.IO.Path.Combine(value, "GTAVLauncher.exe");
                    //isSteam = !File.Exists(System.IO.Path.Combine(value, "PlayGTAV.exe"));
                    installationPath = value;
                }
                else
                {
                    Console.WriteLine("GTAV Not installed!");
                }
            }
        }

        public static string InstallPath(bool edit)
        {
            try
            {
                if (File.Exists(System.IO.Path.Combine(Environment.CurrentDirectory, "GTA5.exe")))
                {
                    //isSteam = !File.Exists(System.IO.Path.Combine(Environment.CurrentDirectory, "PlayGTAV.exe"));
                    return Environment.CurrentDirectory;
                }

                string path = string.Empty;
                using (RegistryKey key = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node\rockstar games\Grand Theft Auto V", edit))
                {

                    if (key != null)
                    {
                        path = key.GetValue("InstallFolder", "").ToString();
                    }

                    if (!string.IsNullOrEmpty(path))
                    {
                        isSteam = false;
                        return path;
                    }

                }

                using (RegistryKey key = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\Steam App 271590", edit))
                {
                    if (key != null)
                    {
                        path = key.GetValue("InstallLocation", "").ToString();
                    }

                    if (!string.IsNullOrEmpty(path))
                    {
                        isSteam = true;
                        return path;
                    }
                }

                return string.Empty;
            }
            catch
            {
                Console.WriteLine("GTAV Not Installed!");
                return string.Empty;
            }
        }
        public static bool startGTA(bool mods, string commandline)
        {
            Directory.SetCurrentDirectory(AppDomain.CurrentDomain.BaseDirectory);
            CurrentInstallationPath = InstallPath(false);
            if (mods)
            {
                if (!enableMods())
                    return false;
            }
            else
                if (!disableMods())
                return false;

            return start(commandline);
        }

        private static bool start(string mode)
        {
            try
            {
                if (!isSteam)
                        Process.Start(binaryPath);
                else
                    Process.Start("steam://rungameid/271590");
                return true;

            }
            catch (Exception ex)
            {
                Console.WriteLine("GTAV not started: " + ex);
                return false;
            }
        }

        private static string[] loads = { "scripthookv.dll", "dsound.dll", "dinput8.dll", "ScriptHookVDotNet.dll" };

        private static bool moveFile(string fileLocation, string fileDestination, bool delete)
        {
            try
            {
                File.Delete(fileDestination);
                if (File.Exists(fileDestination))
                    throw new Exception("Destination File already exists.");
                File.Move(fileLocation, fileDestination);
                //File.Copy(fileLocation, fileDestination, true);
                if (delete)
                    File.Delete(fileLocation);
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("There was an error copying file {0}, do you want to continue?\n{1}", System.IO.Path.GetFileName(fileLocation), ex.Message));
                return false;
            }
            return true;
        }
        private static bool disableMods()
        {
            if (!Directory.Exists(modStorePath))
                Directory.CreateDirectory(modStorePath);

            foreach (string loader in loads)
            {

                string loaderLocation = System.IO.Path.Combine(CurrentInstallationPath, loader);
                if (!File.Exists(loaderLocation))
                    continue;
                string loaderCopylocation = System.IO.Path.Combine(modStorePath, loader);
                if (!moveFile(loaderLocation, loaderCopylocation, true))
                    return false;
            }

            foreach (string file in Directory.GetFiles(CurrentInstallationPath, "*.*", SearchOption.TopDirectoryOnly))
            {
                string filename = System.IO.Path.GetFileName(file).ToLower();
                if (filename.Contains(".asi"))
                {
                    if (!moveFile(file, System.IO.Path.Combine(modStorePath, filename), true))
                        return false;
                }
            }

            return true;
        }

        private static bool enableMods()
        {
            if (Directory.Exists(modStorePath))
            {
                foreach (string file in Directory.GetFiles(modStorePath, "*.*", SearchOption.TopDirectoryOnly))
                {
                    string filename = System.IO.Path.GetFileName(file).ToLower();
                    if (!moveFile(file, System.IO.Path.Combine(CurrentInstallationPath, filename), false))
                        return false;
                }
            }
            return true;
        }
    }
}
