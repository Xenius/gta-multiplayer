﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace HL_Launcher
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static Boolean remember = false;

        static List<String[]> list = new List<string[]>();
        static int index = 0;

        static WebClient wc = new WebClient();

        static Boolean canceled = false;

        public MainWindow()
        {
            InitializeComponent();

            label3.Content = settings.version;

            wc.Proxy = null;
            string version = "";
            if (File.Exists("Updater.exe"))
                File.Delete("Updater.exe");
            try {
                version = wc.DownloadString(new Uri(settings.web + "version.php?auth=" + settings.auth));
            }
            catch
            {
                MessageBox.Show("A launcher szerver nem elérhető!");
                this.Close();
            }

            if (version != settings.version)
            {
                string file_path = (Assembly.GetEntryAssembly().Location).Replace(" ", "%");
                WebClient wc = new WebClient();
                wc.Proxy = null;
                try
                {
                    wc.DownloadFileAsync(new Uri(settings.web + "getfile.php?auth=" + settings.auth + "&file=updater"), "Updater.exe");
                }
                catch
                {
                    MessageBox.Show("Nem lehet kapcsolódni a szerverhez!");
                    this.Close();
                }
                wc.DownloadFileCompleted += delegate {
                    try
                    {
                        Process.Start("Updater.exe", settings.web + " " + settings.auth + " " + file_path + " " + settings.version);
                    }
                    catch
                    {
                        MessageBox.Show("Nem lehet elindítani a frissítőt!");
                    }
                    this.Close();
                };
                this.Visibility = Visibility.Hidden;
            }

            if (!Directory.Exists(settings.dir))
                Directory.CreateDirectory(settings.dir);
            image1.Source = new ImageSourceConverter().ConvertFromString(@"pack://application:,,,/X.png") as ImageSource;
            image2.Source = new ImageSourceConverter().ConvertFromString(@"pack://application:,,,/_.png") as ImageSource;
            image3.Source = new ImageSourceConverter().ConvertFromString(@"pack://application:,,,/rm.png") as ImageSource;
            image4.Source = new ImageSourceConverter().ConvertFromString(@"pack://application:,,,/BTN.png") as ImageSource;
            image5.Source = new ImageSourceConverter().ConvertFromString(@"pack://application:,,,/cancel.png") as ImageSource;

            hideUser();
            showLogin();

            wc.DownloadProgressChanged += new DownloadProgressChangedEventHandler(DownloadProgressCallback);
            wc.DownloadFileCompleted += delegate {
                index++;
                startDownload(index);
                updateProgress();
            };
        }

        string getInstallPath()
        {
            RegistryKey myKey = Registry.LocalMachine.OpenSubKey(@"Software\HungaryLife", false);
            
            if (myKey.GetValue("InstallPath") != null)
            {
                return myKey.GetValue("InstallPath").ToString();
            }
            else
            {
                myKey.SetValue("InstallPath", settings.dir);

                return settings.dir;
            }
        }

        private void DownloadProgressCallback(object sender, DownloadProgressChangedEventArgs e)
        {
            if (list.Count > index)
            {
                float sz = (float)280 * ((float)e.BytesReceived / (float)e.TotalBytesToReceive);
                imageP2.Width = sz;
                label1_Copy.Content = "Letöltés alatt: " + list[index][0];
                label1_Copy1.Content = SizeSuffix(e.BytesReceived) + "/" + SizeSuffix(e.TotalBytesToReceive);
            }
        }

        private void updateProgress()
        {
            if (list.Count <= 0)
                return;
            float percent = (int)(((100f * index) / list.Count));
            label1.Content = "Teljes haladás: " + percent + "%" + " (" + index + "/" + list.Count + ")";
            imageP1.Width = 280 * (percent / 100);
        }

        private void loadPatchList()
        {
            label1.Content = "Patcher lista betöltése...";
            WebClient wList = new WebClient();
            wList.Proxy = null;
            try
            {
                wList.DownloadFileAsync(new Uri(settings.web + "list.php?auth=" + settings.auth + "&game=gtav"), settings.dir + "patch.list");
            }
            catch
            {
                MessageBox.Show("Nem lehet kapcsolódni a patcher szerverhez!");
                return;
            }
            wList.DownloadFileCompleted += delegate
            {
                Boolean mappa = false;
                StreamReader file = new StreamReader(settings.dir + "patch.list");
                while (file.Peek() != -1)
                {
                    string sor = file.ReadLine();
                    if (sor == "[DIRECTORY]")
                    {
                        mappa = true;
                    }
                    else if (sor == "[/DIRECTORY]")
                    {
                        mappa = false;
                    }
                    else
                    {
                        string[] ertekek = sor.Split('|');
                        if (mappa)
                        {
                            if (!Directory.Exists(settings.dir + ertekek[0]))
                            {
                                label1.Content = "Mappa létrehozás: " + ertekek[0];
                                Directory.CreateDirectory(settings.dir + ertekek[0]);
                            }
                        }
                        else
                        {
                            list.Add(ertekek);
                        }
                    }
                }
                file.Close();
                File.Delete(settings.dir + "patch.list");
                startDownload(0);
                index = 0;
            };
        }

        private void startDownload(int i)
        {
            if (i < list.Count)
            {
                string[] data = list[i];
                string path = (settings.dir + data[0]).Replace("\\", "/");
                string url = (settings.web + "downloads/" + data[0]).Replace("\\", "/");
                Boolean download = true;

                if (File.Exists(path))
                {
                    string downMD5 = data[1];
                    string MD5 = MD5FileHash(path);
                    if (downMD5 == MD5)
                        download = false;
                    else
                        File.Delete(path);
                }
                if (download)
                {
                    updateProgress();
                    if (File.Exists(path))
                        File.Delete(path);
                    try
                    {
                        wc.DownloadFileAsync(new Uri(url), path);
                    }
                    catch
                    {
                        label1.Content = "Letöltési hiba: " + data[1];
                    }
                }
                else
                {
                    label1.Content = "Rendben van: " + data[0];
                    index++;
                    startDownload(index);
                    updateProgress();
                }
            }
            else
            {
                if (canceled)
                    return;
                label1.Content = "Játék indítása...";
                GTAV.startGTA(true, "-scOfflineOnly");
                //Process.Start(settings.dir + "Multi Theft Auto.exe", "127.0.0.1:22003");
                //this.Close();
            }
        }

        static readonly string[] SizeSuffixes = { "bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };
        static string SizeSuffix(Int64 value)
        {
            if (value < 0) { return "-" + SizeSuffix(-value); }
            if (value == 0) { return "0.0 bytes"; }

            int mag = (int)Math.Log(value, 1024);
            decimal adjustedSize = (decimal)value / (1L << (mag * 10));

            return string.Format("{0:n1} {1}", adjustedSize, SizeSuffixes[mag]);
        }

        public static string MD5FileHash(string filename)
        {
            using (FileStream stream = new FileStream(filename, FileMode.Open, FileAccess.Read))
            {
                using (MD5 md5 = new MD5CryptoServiceProvider())
                {
                    byte[] hash = md5.ComputeHash(stream);
                    StringBuilder builder = new StringBuilder();
                    foreach (byte b in hash)
                    {
                        builder.AppendFormat("{0:x2}", b);
                    }
                    return builder.ToString();
                }
            }
        }

        void initRemember()
        {
            if (File.Exists(settings.rememberFile))
            {
                StreamReader fajl = new StreamReader(settings.rememberFile);
                Boolean toggle = false;
                try {
                    textBox.Text = Base64Decode(fajl.ReadLine());
                    passwordBox.Password = Base64Decode(fajl.ReadLine());
                    toggle = true;
                }
                catch {
                    toggle = false;
                }
                if (toggle)
                {
                    remember = true;
                    image3.Source = new ImageSourceConverter().ConvertFromString(@"pack://application:,,,/rm_hover.png") as ImageSource;
                }
            }
        }

        void ifSaveRemember()
        {
            if (File.Exists(settings.rememberFile))
                File.Delete(settings.rememberFile);
            if (remember)
                File.WriteAllText(settings.rememberFile, Base64Encode(textBox.Text) + Environment.NewLine + Base64Encode(passwordBox.Password));
        }

        void hideLogin()
        {
            image3.Visibility = Visibility.Hidden;
            image4.Visibility = Visibility.Hidden;
            textBox.Visibility = Visibility.Hidden;
            passwordBox.Visibility = Visibility.Hidden;
        }
        void showLogin()
        {
            this.Background = new ImageBrush(new BitmapImage(new Uri(@"pack://application:,,,/back.png")));

            image3.Visibility = Visibility.Visible;
            image4.Visibility = Visibility.Visible;
            textBox.Visibility = Visibility.Visible;
            passwordBox.Visibility = Visibility.Visible;
            label2.Visibility = Visibility.Hidden;

            textBox.Text = "Felhasználónév";
            passwordBox.Password = "Jelszó";
            initRemember();
        }

        void hideUser()
        {
            label.Visibility = Visibility.Hidden;
            label_Copy.Visibility = Visibility.Hidden;
            label_Copy1.Visibility = Visibility.Hidden;
            label1.Visibility = Visibility.Hidden;
            label1_Copy.Visibility = Visibility.Hidden;
            imageP1.Visibility = Visibility.Hidden;
            imageP2.Visibility = Visibility.Hidden;
            image5.Visibility = Visibility.Hidden;
            label1_Copy1.Visibility = Visibility.Hidden;
        }

        void showUser(string username, string hlp)
        {
            this.Background = new ImageBrush(new BitmapImage(new Uri(@"pack://application:,,,/back2.png")));

            label.Content = "Üdv, " + username + "!";
            label_Copy.Content = hlp;
            label.Visibility = Visibility.Visible;
            label_Copy.Visibility = Visibility.Visible;
            label_Copy1.Visibility = Visibility.Visible;
            label1.Visibility = Visibility.Visible;
            label1_Copy.Visibility = Visibility.Visible;
            label1.Content = "Teljes haladás: 0%";
            label1_Copy.Content = "Letöltés atatt: n/a";
            label2.Visibility = Visibility.Hidden;
            imageP1.Visibility = Visibility.Visible;
            imageP2.Visibility = Visibility.Visible;
            imageP1.Width = 0;
            imageP2.Width = 0;
            image5.Visibility = Visibility.Visible;
            label1_Copy1.Visibility = Visibility.Visible;
        }

        private void image1_MouseEnter(object sender, MouseEventArgs e)
        {
            image1.Source = new ImageSourceConverter().ConvertFromString(@"pack://application:,,,/X_Hover.png") as ImageSource;
        }

        private void image1_MouseLeave(object sender, MouseEventArgs e)
        {
            image1.Source = new ImageSourceConverter().ConvertFromString(@"pack://application:,,,/X.png") as ImageSource;
        }

        private void image1_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.Close();
        }

        private void image2_MouseEnter(object sender, MouseEventArgs e)
        {
            image2.Source = new ImageSourceConverter().ConvertFromString(@"pack://application:,,,/__Hover.png") as ImageSource;
        }

        private void image2_MouseLeave(object sender, MouseEventArgs e)
        {
            image2.Source = new ImageSourceConverter().ConvertFromString(@"pack://application:,,,/_.png") as ImageSource;
        }

        private void image2_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void image3_MouseEnter(object sender, MouseEventArgs e)
        {
            image3.Source = new ImageSourceConverter().ConvertFromString(@"pack://application:,,,/rm_hover.png") as ImageSource;
        }

        private void image3_MouseLeave(object sender, MouseEventArgs e)
        {
            if (!remember)
                image3.Source = new ImageSourceConverter().ConvertFromString(@"pack://application:,,,/rm.png") as ImageSource;
        }

        private void image3_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            remember = !remember;
            if (remember)
                image3.Source = new ImageSourceConverter().ConvertFromString(@"pack://application:,,,/rm_hover.png") as ImageSource;
            else
                image3.Source = new ImageSourceConverter().ConvertFromString(@"pack://application:,,,/rm.png") as ImageSource;
        }

        private void image4_MouseEnter(object sender, MouseEventArgs e)
        {
            image4.Source = new ImageSourceConverter().ConvertFromString(@"pack://application:,,,/BTN Hover.png") as ImageSource;
        }

        private void image4_MouseLeave(object sender, MouseEventArgs e)
        {
            image4.Source = new ImageSourceConverter().ConvertFromString(@"pack://application:,,,/BTN.png") as ImageSource;
        }

        private void image4_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
           
            try
            {
                string re = wc.DownloadString(new Uri(settings.web + "login.php?auth=" + settings.auth + "&user=" + textBox.Text + "&pass=" + passwordBox.Password)); ;
                try {
                    string[] tags = re.Split('#');
                    ifSaveRemember();
                    showUser(tags[1].Replace("_", " "), tags[3]);
                    hideLogin();
                    canceled = false;
                    loadPatchList();
                }
                catch
                {
                    label2.Visibility = Visibility.Visible;
                    label2.Content = re;
                }
            }
            catch
            {
                label2.Visibility = Visibility.Visible;
                label2.Content = "Nem lehet kapcsolódni a szerverhez!";
            }
        }

        private void image5_MouseEnter(object sender, MouseEventArgs e)
        {
            image5.Source = new ImageSourceConverter().ConvertFromString(@"pack://application:,,,/cancel_hover.png") as ImageSource;
        }

        private void image5_MouseLeave(object sender, MouseEventArgs e)
        {
            image5.Source = new ImageSourceConverter().ConvertFromString(@"pack://application:,,,/cancel.png") as ImageSource;
        }

        private void image5_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            list = new List<string[]>();
            index = 0;
            canceled = true;
            hideUser();
            showLogin();
        }

        private void Border_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void textBox_GotFocus(object sender, RoutedEventArgs e)
        {
            if (textBox.Text == "Felhasználónév")
                textBox.Text = "";
        }

        private void textBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (textBox.Text == "")
                textBox.Text = "Felhasználónév";
        }

        private void passwordBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (passwordBox.Password == "")
                passwordBox.Password = "Jelszó";
        }

        private void passwordBox_GotFocus(object sender, RoutedEventArgs e)
        {
            if (passwordBox.Password == "Jelszó")
                passwordBox.Password = "";
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
    }
}
