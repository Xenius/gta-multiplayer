﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using GTA;
using GTA.Math;
using GTA.Native;
using NativeUI;
using Font = GTA.Font;

namespace GTACoOp
{
    public class Scripting
    {
        public static void clearThisFrame()
        {
                        Function.Call(Hash.SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME, 0f);
            Function.Call(Hash.SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME, 0f);
            Function.Call(Hash.SET_PARKED_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME, 0f);

            Function.Call(Hash.SET_PED_DENSITY_MULTIPLIER_THIS_FRAME, 0f);
            Function.Call(Hash.SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME, 0f, 0f);
        }

        public static void clearGTAVMap()
        {
            //UI.Notify("Try clear!");

            var pos = Game.Player.Character.Position;
            Function.Call(Hash.CLEAR_AREA_OF_PEDS, pos.X, pos.Y, pos.Z, 10000f, 0);
            Function.Call(Hash.CLEAR_AREA_OF_VEHICLES, pos.X, pos.Y, pos.Z, 10000f, 0);
            Function.Call(Hash.CLEAR_AREA_OF_PROJECTILES, pos.X, pos.Y, pos.Z, 10000f, 0);
            Function.Call(Hash.SET_GARBAGE_TRUCKS, 0);
            Function.Call(Hash.SET_RANDOM_BOATS, 0);
            Function.Call(Hash.SET_RANDOM_TRAINS, 0);

            Function.Call((Hash)0x2F9A292AD0A3BD89);
            Function.Call((Hash)0x5F3B7749C112D552);

            Function.Call(Hash.SET_TIME_SCALE, 1f);

            Blip blip = World.GetActiveBlips()[1];
            UI.ShowSubtitle(blip.ToString(), 2000);
            /*Blip blip = World.GetActiveBlips().FirstOrDefault(b => b.Type == 4);
            if (blip != null)
            {
                Function.Call(Hash.SET_BLIP_SCALE, blip, 0);
                //Game.Player.Character.Position = blip.Position;
                UI.ShowSubtitle("Done", 2000);
            }
            else
            {
                UI.ShowSubtitle("Not found", 2000);
            }*/

            /* int blipCount = Function.Call<int>(Hash.GET_NUMBER_OF_ACTIVE_BLIPS, 0);
             for (int i = 1; i <= blipCount; i++)
             {
                 Function.Call(Hash.SET_BLIP_SCALE, i, 2);
             }
             UI.Notify("Blips removed: " + blipCount);*/
            /*Blip[] AllBlips = World.GetActiveBlips();
            foreach (Blip blip in AllBlips)
            {
                UI.Notify("Blip found");
            }*/
        }

        // SKIN START
        public static List<PedHash> skinList = new List<PedHash>() {
            PedHash.Michael, PedHash.Franklin, PedHash.Trevor, PedHash.Abigail, PedHash.AmandaTownley, PedHash.Andreas, PedHash.Ashley, PedHash.Ballasog, PedHash.Bankman, PedHash.Barry, PedHash.Bestmen, PedHash.Beverly, PedHash.Brad, PedHash.Bride, PedHash.Car3Guy1, PedHash.Car3Guy2, PedHash.Casey, PedHash.Chef, PedHash.Clay, PedHash.Claypain, PedHash.Cletus, PedHash.CrisFormage, PedHash.Dale, PedHash.DaveNorton, PedHash.Denise, PedHash.Devin, PedHash.Dom, PedHash.Dreyfuss, PedHash.DrFriedlander, PedHash.Fabien, PedHash.FbiSuit01, PedHash.Floyd, PedHash.Groom, PedHash.Hao, PedHash.Hunter, PedHash.Janet, PedHash.JayNorris, PedHash.Jewelass, PedHash.JimmyBoston, PedHash.JimmyDisanto, PedHash.JoeMinuteman, PedHash.JohnnyKlebitz, PedHash.Josef, PedHash.Josh, PedHash.KerryMcintosh, PedHash.LamarDavis, PedHash.Lazlow, PedHash.LesterCrest, PedHash.Lifeinvad01, PedHash.Lifeinvad02, PedHash.Magenta, PedHash.Manuel, PedHash.Marnie, PedHash.MaryAnn, PedHash.Maude, PedHash.Michelle, PedHash.Milton, PedHash.Molly, PedHash.MrK, PedHash.MrsPhillips, PedHash.MrsThornhill, PedHash.Natalia, PedHash.NervousRon, PedHash.Nigel, PedHash.OldMan1a, PedHash.OldMan2, PedHash.Omega, PedHash.ONeil, PedHash.Orleans, PedHash.Ortega, PedHash.Paper, PedHash.Patricia, PedHash.Priest, PedHash.PrologueDriver, PedHash.PrologueSec01, PedHash.PrologueSec02, PedHash.RampGang, PedHash.RampHic, PedHash.RampHipster, PedHash.RampMex, PedHash.RoccoPelosi, PedHash.RussianDrunk, PedHash.ScreenWriter, PedHash.SiemonYetarian, PedHash.Solomon, PedHash.SteveHains, PedHash.Stretch, PedHash.Talina, PedHash.Tanisha, PedHash.TaoCheng, PedHash.TaosTranslator, PedHash.TennisCoach, PedHash.Terry, PedHash.TomEpsilon, PedHash.Tonya, PedHash.TracyDisanto, PedHash.TrafficWarden, PedHash.TylerDixon, PedHash.Wade, PedHash.WeiCheng, PedHash.Zimbor, PedHash.AbigailCutscene, PedHash.AmandaTownleyCutscene, PedHash.AndreasCutscene, PedHash.AnitaCutscene, PedHash.AntonCutscene, PedHash.AshleyCutscene, PedHash.BallasogCutscene, PedHash.BankmanCutscene, PedHash.BarryCutscene, PedHash.BeverlyCutscene, PedHash.BradCutscene, PedHash.BradCadaverCutscene, PedHash.BrideCutscene, PedHash.BurgerDrugCutscene, PedHash.Car3Guy1Cutscene, PedHash.Car3Guy2Cutscene, PedHash.CarBuyerCutscene, PedHash.CaseyCutscene, PedHash.ChefCutscene, PedHash.ChinGoonCutscene, PedHash.ClayCutscene, PedHash.CletusCutscene, PedHash.CopCutscene, PedHash.CrisFormageCutscene, PedHash.CustomerCutscene, PedHash.DaleCutscene, PedHash.DaveNortonCutscene, PedHash.DebraCutscene, PedHash.DeniseCutscene, PedHash.DeniseFriendCutscene, PedHash.DevinCutscene, PedHash.DomCutscene, PedHash.DreyfussCutscene, PedHash.DrFriedlanderCutscene, PedHash.FabienCutscene, PedHash.FbiSuit01Cutscene, PedHash.FloydCutscene, PedHash.FosRepCutscene, PedHash.GCutscene, PedHash.GroomCutscene, PedHash.GroveStrDlrCutscene, PedHash.GuadalopeCutscene, PedHash.GurkCutscene, PedHash.HaoCutscene, PedHash.HughCutscene, PedHash.HunterCutscene, PedHash.ImranCutscene, PedHash.JanetCutscene, PedHash.JanitorCutscene, PedHash.JewelassCutscene, PedHash.JimmyBostonCutscene, PedHash.JimmyDisantoCutscene, PedHash.JoeMinutemanCutscene, PedHash.JohnnyKlebitzCutscene, PedHash.JosefCutscene, PedHash.JoshCutscene, PedHash.LamarDavisCutscene, PedHash.LazlowCutscene, PedHash.LesterCrestCutscene, PedHash.Lifeinvad01Cutscene, PedHash.MagentaCutscene, PedHash.ManuelCutscene, PedHash.MarnieCutscene, PedHash.MartinMadrazoCutscene, PedHash.MaryannCutscene, PedHash.MaudeCutscene, PedHash.MerryWeatherCutscene, PedHash.MichelleCutscene, PedHash.MiltonCutscene, PedHash.MollyCutscene, PedHash.MoviePremFemaleCutscene, PedHash.MoviePremMaleCutscene, PedHash.MrKCutscene, PedHash.MrsPhillipsCutscene, PedHash.MrsThornhillCutscene, PedHash.NataliaCutscene, PedHash.NervousRonCutscene, PedHash.NigelCutscene, PedHash.OldMan1aCutscene, PedHash.OldMan2Cutscene, PedHash.OmegaCutscene, PedHash.OrleansCutscene, PedHash.OrtegaCutscene, PedHash.OscarCutscene, PedHash.PaperCutscene, PedHash.PatriciaCutscene, PedHash.PornDudesCutscene, PedHash.PriestCutscene, PedHash.PrologueDriverCutscene, PedHash.PrologueSec01Cutscene, PedHash.PrologueSec02Cutscene, PedHash.RampGangCutscene, PedHash.RampHicCutscene, PedHash.RampHipsterCutscene, PedHash.RampMarineCutscene, PedHash.RampMexCutscene, PedHash.ReporterCutscene, PedHash.RoccoPelosiCutscene, PedHash.RussianDrunkCutscene, PedHash.ScreenWriterCutscene, PedHash.SiemonYetarianCutscene, PedHash.SolomonCutscene, PedHash.SteveHainsCutscene, PedHash.StretchCutscene, PedHash.Stripper01Cutscene, PedHash.Stripper02Cutscene, PedHash.TanishaCutscene, PedHash.TaoChengCutscene, PedHash.TaosTranslatorCutscene, PedHash.TennisCoachCutscene, PedHash.TerryCutscene, PedHash.TomCutscene, PedHash.TomEpsilonCutscene, PedHash.TonyaCutscene, PedHash.TracyDisantoCutscene, PedHash.TrafficWardenCutscene, PedHash.WadeCutscene, PedHash.WeiChengCutscene, PedHash.ZimborCutscene, PedHash.Boar, PedHash.Cat, PedHash.ChickenHawk, PedHash.Chimp, PedHash.Chop, PedHash.Cormorant, PedHash.Cow, PedHash.Coyote, PedHash.Crow, PedHash.Deer, PedHash.Dolphin, PedHash.Fish, PedHash.Hen, PedHash.HammerShark, PedHash.Humpback, PedHash.Husky, PedHash.KillerWhale, PedHash.MountainLion, PedHash.Pig, PedHash.Pigeon, PedHash.Poodle, PedHash.Pug, PedHash.Rabbit, PedHash.Rat, PedHash.Retriever, PedHash.Rhesus, PedHash.Rottweiler, PedHash.Seagull, PedHash.Shepherd, PedHash.Stingray, PedHash.TigerShark, PedHash.Westy, PedHash.Abner, PedHash.AlDiNapoli, PedHash.Antonb, PedHash.Armoured01, PedHash.Babyd, PedHash.Bankman01, PedHash.Baygor, PedHash.BikeHire01, PedHash.BikerChic, PedHash.BurgerDrug, PedHash.Chip, PedHash.Claude01, PedHash.ComJane, PedHash.Corpse01, PedHash.Corpse02, PedHash.Cyclist01, PedHash.DeadHooker, PedHash.ExArmy01, PedHash.Famdd01, PedHash.FibArchitect, PedHash.FibMugger01, PedHash.FibSec01, PedHash.FilmDirector, PedHash.Finguru01, PedHash.FreemodeFemale01, PedHash.FreemodeMale01, PedHash.Glenstank01, PedHash.Griff01, PedHash.Guido01, PedHash.GunVend01, PedHash.Hacker, PedHash.Hippie01, PedHash.Hotposh01, PedHash.Imporage, PedHash.Jesus01, PedHash.Jewelass01, PedHash.JewelSec01, PedHash.JewelThief, PedHash.Justin, PedHash.Mani, PedHash.Markfost, PedHash.Marston01, PedHash.MilitaryBum, PedHash.Miranda, PedHash.Mistress, PedHash.Misty01, PedHash.MovieStar, PedHash.MPros01, PedHash.Niko01, PedHash.Paparazzi, PedHash.Party01, PedHash.PartyTarget, PedHash.PestContDriver, PedHash.PestContGunman, PedHash.Pogo01, PedHash.Poppymich, PedHash.Princess, PedHash.Prisoner01, PedHash.PrologueHostage01, PedHash.PrologueMournFemale01, PedHash.PrologueMournMale01, PedHash.RivalPaparazzi, PedHash.ShopKeep01, PedHash.SpyActor, PedHash.SpyActress, PedHash.StripperLite, PedHash.Taphillbilly, PedHash.Tramp01, PedHash.WillyFist, PedHash.Zombie01, PedHash.Acult01AMM, PedHash.Acult01AMO, PedHash.Acult01AMY, PedHash.Acult02AMO, PedHash.Acult02AMY, PedHash.AfriAmer01AMM, PedHash.Airhostess01SFY, PedHash.AirworkerSMY, PedHash.Ammucity01SMY, PedHash.AmmuCountrySMM, PedHash.ArmBoss01GMM, PedHash.ArmGoon01GMM, PedHash.ArmGoon02GMY, PedHash.ArmLieut01GMM, PedHash.Armoured01SMM, PedHash.Armoured02SMM, PedHash.Armymech01SMY, PedHash.Autopsy01SMY, PedHash.Autoshop01SMM, PedHash.Autoshop02SMM, PedHash.Azteca01GMY, PedHash.BallaEast01GMY, PedHash.BallaOrig01GMY, PedHash.Ballas01GFY, PedHash.BallaSout01GMY, PedHash.Barman01SMY, PedHash.Bartender01SFY, PedHash.Baywatch01SFY, PedHash.Baywatch01SMY, PedHash.Beach01AFM, PedHash.Beach01AFY, PedHash.Beach01AMM, PedHash.Beach01AMO, PedHash.Beach01AMY, PedHash.Beach02AMM, PedHash.Beach02AMY, PedHash.Beach03AMY, PedHash.Beachvesp01AMY, PedHash.Beachvesp02AMY, PedHash.Bevhills01AFM, PedHash.Bevhills01AFY, PedHash.Bevhills01AMM, PedHash.Bevhills01AMY, PedHash.Bevhills02AFM, PedHash.Bevhills02AFY, PedHash.Bevhills02AMM, PedHash.Bevhills02AMY, PedHash.Bevhills03AFY, PedHash.Bevhills04AFY, PedHash.Blackops01SMY, PedHash.Blackops02SMY, PedHash.Bodybuild01AFM, PedHash.Bouncer01SMM, PedHash.Breakdance01AMY, PedHash.Busboy01SMY, PedHash.Busicas01AMY, PedHash.Business01AFY, PedHash.Business01AMM, PedHash.Business01AMY, PedHash.Business02AFM, PedHash.Business02AFY, PedHash.Business02AMY, PedHash.Business03AFY, PedHash.Business03AMY, PedHash.Business04AFY, PedHash.Busker01SMO, PedHash.Chef01SMY, PedHash.ChemSec01SMM, PedHash.ChemWork01GMM, PedHash.ChiBoss01GMM, PedHash.ChiCold01GMM, PedHash.ChiGoon01GMM, PedHash.ChiGoon02GMM, PedHash.CiaSec01SMM, PedHash.Clown01SMY, PedHash.Cntrybar01SMM, PedHash.Construct01SMY, PedHash.Construct02SMY, PedHash.Cop01SFY, PedHash.Cop01SMY, PedHash.Cyclist01AMY, PedHash.Dealer01SMY, PedHash.Devinsec01SMY, PedHash.Dhill01AMY, PedHash.Dockwork01SMM, PedHash.Dockwork01SMY, PedHash.Doctor01SMM, PedHash.Doorman01SMY, PedHash.Downtown01AFM, PedHash.Downtown01AMY, PedHash.DwService01SMY, PedHash.DwService02SMY, PedHash.Eastsa01AFM, PedHash.Eastsa01AFY, PedHash.Eastsa01AMM, PedHash.Eastsa01AMY, PedHash.Eastsa02AFM, PedHash.Eastsa02AFY, PedHash.Eastsa02AMM, PedHash.Eastsa02AMY, PedHash.Eastsa03AFY, PedHash.Epsilon01AFY, PedHash.Epsilon01AMY, PedHash.Epsilon02AMY, PedHash.Factory01SFY, PedHash.Factory01SMY, PedHash.Famca01GMY, PedHash.Famdnf01GMY, PedHash.Famfor01GMY, PedHash.Families01GFY, PedHash.Farmer01AMM, PedHash.FatBla01AFM, PedHash.FatCult01AFM, PedHash.Fatlatin01AMM, PedHash.FatWhite01AFM, PedHash.FemBarberSFM, PedHash.FibOffice01SMM, PedHash.FibOffice02SMM, PedHash.Fireman01SMY, PedHash.Fitness01AFY, PedHash.Fitness02AFY, PedHash.Gaffer01SMM, PedHash.GarbageSMY, PedHash.Gardener01SMM, PedHash.Gay01AMY, PedHash.Gay02AMY, PedHash.Genfat01AMM, PedHash.Genfat02AMM, PedHash.Genhot01AFY, PedHash.Genstreet01AFO, PedHash.Genstreet01AMO, PedHash.Genstreet01AMY, PedHash.Genstreet02AMY, PedHash.GentransportSMM, PedHash.Golfer01AFY, PedHash.Golfer01AMM, PedHash.Golfer01AMY, PedHash.Grip01SMY, PedHash.Hairdress01SMM, PedHash.Hasjew01AMM, PedHash.Hasjew01AMY, PedHash.Highsec01SMM, PedHash.Highsec02SMM, PedHash.Hiker01AFY, PedHash.Hiker01AMY, PedHash.Hillbilly01AMM, PedHash.Hillbilly02AMM, PedHash.Hippie01AFY, PedHash.Hippy01AMY, PedHash.Hipster01AFY, PedHash.Hipster01AMY, PedHash.Hipster02AFY, PedHash.Hipster02AMY, PedHash.Hipster03AFY, PedHash.Hipster03AMY, PedHash.Hipster04AFY, PedHash.Hooker01SFY, PedHash.Hooker02SFY, PedHash.Hooker03SFY, PedHash.Hwaycop01SMY, PedHash.Indian01AFO, PedHash.Indian01AFY, PedHash.Indian01AMM, PedHash.Indian01AMY, PedHash.JanitorSMM, PedHash.Jetski01AMY, PedHash.Juggalo01AFY, PedHash.Juggalo01AMY, PedHash.KorBoss01GMM, PedHash.Korean01GMY, PedHash.Korean02GMY, PedHash.KorLieut01GMY, PedHash.Ktown01AFM, PedHash.Ktown01AFO, PedHash.Ktown01AMM, PedHash.Ktown01AMO, PedHash.Ktown01AMY, PedHash.Ktown02AFM, PedHash.Ktown02AMY, PedHash.Lathandy01SMM, PedHash.Latino01AMY, PedHash.Lifeinvad01SMM, PedHash.LinecookSMM, PedHash.Lost01GFY, PedHash.Lost01GMY, PedHash.Lost02GMY, PedHash.Lost03GMY, PedHash.Lsmetro01SMM, PedHash.Maid01SFM, PedHash.Malibu01AMM, PedHash.Mariachi01SMM, PedHash.Marine01SMM, PedHash.Marine01SMY, PedHash.Marine02SMM, PedHash.Marine02SMY, PedHash.Marine03SMY, PedHash.Methhead01AMY, PedHash.MexBoss01GMM, PedHash.MexBoss02GMM, PedHash.MexCntry01AMM, PedHash.MexGang01GMY, PedHash.MexGoon01GMY, PedHash.MexGoon02GMY, PedHash.MexGoon03GMY, PedHash.MexLabor01AMM, PedHash.MexThug01AMY, PedHash.Migrant01SFY, PedHash.Migrant01SMM, PedHash.MimeSMY, PedHash.Motox01AMY, PedHash.Motox02AMY, PedHash.MovAlien01, PedHash.MovPrem01SFY, PedHash.Movprem01SMM, PedHash.Movspace01SMM, PedHash.Musclbeac01AMY, PedHash.Musclbeac02AMY, PedHash.OgBoss01AMM, PedHash.Paparazzi01AMM, PedHash.Paramedic01SMM, PedHash.PestCont01SMY, PedHash.Pilot01SMM, PedHash.Pilot01SMY, PedHash.Pilot02SMM, PedHash.PoloGoon01GMY, PedHash.PoloGoon02GMY, PedHash.Polynesian01AMM, PedHash.Polynesian01AMY, PedHash.Postal01SMM, PedHash.Postal02SMM, PedHash.Prisguard01SMM, PedHash.PrisMuscl01SMY, PedHash.Prisoner01SMY, PedHash.PrologueHostage01AFM, PedHash.PrologueHostage01AMM, PedHash.Ranger01SFY, PedHash.Ranger01SMY, PedHash.Roadcyc01AMY, PedHash.Robber01SMY, PedHash.RsRanger01AMO, PedHash.Runner01AFY, PedHash.Runner01AMY, PedHash.Runner02AMY, PedHash.Rurmeth01AFY, PedHash.Rurmeth01AMM, PedHash.Salton01AFM, PedHash.Salton01AFO, PedHash.Salton01AMM, PedHash.Salton01AMO, PedHash.Salton01AMY, PedHash.Salton02AMM, PedHash.Salton03AMM, PedHash.Salton04AMM, PedHash.SalvaBoss01GMY, PedHash.SalvaGoon01GMY, PedHash.SalvaGoon02GMY, PedHash.SalvaGoon03GMY, PedHash.SbikeAMO, PedHash.Scdressy01AFY, PedHash.Scientist01SMM, PedHash.Scrubs01SFY, PedHash.Security01SMM, PedHash.Sheriff01SFY, PedHash.Sheriff01SMY, PedHash.ShopHighSFM, PedHash.ShopLowSFY, PedHash.ShopMaskSMY, PedHash.ShopMidSFY, PedHash.Skater01AFY, PedHash.Skater01AMM, PedHash.Skater01AMY, PedHash.Skater02AMY, PedHash.Skidrow01AFM, PedHash.Skidrow01AMM, PedHash.Snowcop01SMM, PedHash.Socenlat01AMM, PedHash.Soucent01AFM, PedHash.Soucent01AFO, PedHash.Soucent01AFY, PedHash.Soucent01AMM, PedHash.Soucent01AMO, PedHash.Soucent01AMY, PedHash.Soucent02AFM, PedHash.Soucent02AFO, PedHash.Soucent02AFY, PedHash.Soucent02AMM, PedHash.Soucent02AMO, PedHash.Soucent02AMY, PedHash.Soucent03AFY, PedHash.Soucent03AMM, PedHash.Soucent03AMO, PedHash.Soucent03AMY, PedHash.Soucent04AMM, PedHash.Soucent04AMY, PedHash.Soucentmc01AFM, PedHash.Staggrm01AMO, PedHash.Stbla01AMY, PedHash.Stbla02AMY, PedHash.Stlat01AMY, PedHash.Stlat02AMM, PedHash.Stripper01SFY, PedHash.Stripper02SFY, PedHash.StripperLiteSFY, PedHash.Strperf01SMM, PedHash.Strpreach01SMM, PedHash.StrPunk01GMY, PedHash.StrPunk02GMY, PedHash.Strvend01SMM, PedHash.Strvend01SMY, PedHash.Stwhi01AMY, PedHash.Stwhi02AMY, PedHash.Sunbathe01AMY, PedHash.Surfer01AMY, PedHash.Swat01SMY, PedHash.Sweatshop01SFM, PedHash.Sweatshop01SFY, PedHash.Tattoo01AMO, PedHash.Tennis01AFY, PedHash.Tennis01AMM, PedHash.Topless01AFY, PedHash.Tourist01AFM, PedHash.Tourist01AFY, PedHash.Tourist01AMM, PedHash.Tourist02AFY, PedHash.Tramp01AFM, PedHash.Tramp01AMM, PedHash.Tramp01AMO, PedHash.TrampBeac01AFM, PedHash.TrampBeac01AMM, PedHash.Tranvest01AMM, PedHash.Tranvest02AMM, PedHash.Trucker01SMM, PedHash.Ups01SMM, PedHash.Ups02SMM, PedHash.Uscg01SMY, PedHash.Vagos01GFY, PedHash.Valet01SMY, PedHash.Vindouche01AMY, PedHash.Vinewood01AFY, PedHash.Vinewood01AMY, PedHash.Vinewood02AFY, PedHash.Vinewood02AMY, PedHash.Vinewood03AFY, PedHash.Vinewood03AMY, PedHash.Vinewood04AFY, PedHash.Vinewood04AMY, PedHash.Waiter01SMY, PedHash.WinClean01SMY, PedHash.Xmech01SMY, PedHash.Xmech02SMY, PedHash.Yoga01AFY, PedHash.Yoga01AMY
        };

        public static PedHash getSkinHashByID(int skinID)
        {
            return skinList[skinID];
        }

        public static bool resetPlayerClothes(Player thePlayer)
        {
            return Function.Call<bool>(Hash.SET_PED_DEFAULT_COMPONENT_VARIATION, thePlayer.Character.Handle);
        }

        public static bool setPlayerSkin(Player thePlayer, int skinID)
        {
            var characterModel = new Model(skinList[skinID]);
            characterModel.Request(500);

            if (characterModel.IsInCdImage && characterModel.IsValid)
            {
                while (!characterModel.IsLoaded) Script.Wait(100);

                Function.Call(Hash.SET_PLAYER_MODEL, thePlayer, characterModel.Hash);
                resetPlayerClothes(thePlayer);
            } else {
                return false;
            }
            
            characterModel.MarkAsNoLongerNeeded();
            return true;
        }
        // SKIN END

        // CAMERA, OTHER
        public static Vector3 getCameraPosition()
        {
            return GameplayCamera.Position;
        }        

        public static Vector3 getCameraRotation()
        {
            return GameplayCamera.Rotation;
        }
        
        public static bool setWaypoint(Vector3 pos)
        {
            return Function.Call<bool>(Hash.SET_NEW_WAYPOINT, pos.X, pos.Y);
        }
    }
}