﻿using Lidgren.Network;
using MySql.Data.MySqlClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace GTAServer
{
    public class ServerScript
    {
        public virtual string Name { get; set; }

        public delegate bool CallbackFunction(params object[] argsRest);

        public struct Data
        {
            public Data(object k, CallbackFunction c)
            {
                key = k;
                callback = c;
            }

            public object key { get; private set; }
            public CallbackFunction callback { get; private set; }
        }

        public static List<Data> OnChatMessageEvents = new List<Data>();
        public static List<Data> OnPlayerConnectEvents = new List<Data>();
        public static List<Data> OnPlayerDisconnectEvents = new List<Data>();
        public static List<Data> OnConnectionRefusedEvents = new List<Data>();
        public static List<Data> OnPlayerKilledEvents = new List<Data>();
        public static List<Data> OnTickEvents = new List<Data>();

        public static bool OnChatMessage(Client sender, string message)
        {
            foreach (Data a in OnChatMessageEvents)
                if (a.key == sender || a.key == null)
                    a.callback.Invoke(sender, message);
            return true;
        }

        public static bool OnPlayerConnect(Client player)
        {
            foreach(Data a in OnPlayerConnectEvents)
                if (a.key == player || a.key == null)
                    a.callback.Invoke(player);
            return true;
        }

        public static bool OnPlayerDisconnect(Client player)
        {
            foreach (Data a in OnPlayerDisconnectEvents)
                if (a.key == player || a.key == null)
                    a.callback.Invoke(player);
            return true;
        }

        public static void OnConnectionRefused(Client player, string reason)
        {
            foreach (Data a in OnConnectionRefusedEvents)
                if (a.key == player || a.key == null)
                    a.callback.Invoke(player, reason);
        }

        public static void OnPlayerKilled(Client player)
        {
            foreach (Data a in OnPlayerKilledEvents)
                if (a.key == player || a.key == null)
                    a.callback.Invoke(player);
        }

        public static void OnTick()
        {
            foreach (Data a in OnTickEvents)
            {
                if (a.key == null)
                    a.callback.Invoke();
            }
        }

        public static void outputConsole(string message, int type = 0)
        {
            string text = "";
            if (type == 0)
                text = DateTime.Now.ToString("[HH:mm:ss]", System.Globalization.DateTimeFormatInfo.InvariantInfo) + "[INFO] " + message;
            else if(type == 1)
                text = DateTime.Now.ToString("[HH:mm:ss]", System.Globalization.DateTimeFormatInfo.InvariantInfo) + "[WARNING] " + message;
            else if (type == 2)
                text = DateTime.Now.ToString("[HH:mm:ss]", System.Globalization.DateTimeFormatInfo.InvariantInfo) + "[ERROR] " + message;
            main.addLog(text);
        }

        public static void outputChatBox(string message, Client player = null, string sender = "")
        {
            if (player == null)
                if (sender == "")
                    Program.ServerInstance.SendChatMessageToAll(message);
                else
                    Program.ServerInstance.SendChatMessageToAll(sender, message);
            else
                if(sender == "")
                    Program.ServerInstance.SendChatMessageToPlayer(player, message);
                else
                    Program.ServerInstance.SendChatMessageToPlayer(player, sender, message);
        }

        public static string getPlayerName(Client player)
        {
            return player.Name;
        }

        public static MySqlConnection connectMysql(string myConnectionString)
        {
            MySqlConnection conn = null;
            try
            {
                conn = new MySqlConnection();
                conn.ConnectionString = myConnectionString;
                conn.Open();
            }
            catch (MySqlException ex)
            {
                outputConsole("Mysql connection error: " + ex, 2);
                return null;
            }
            return conn;
        }

        public static MySqlCommand executeQuery(MySqlConnection mysql, string sql)
        {
            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = mysql;
            cmd.CommandText = sql;
            cmd.Prepare();
            cmd.ExecuteNonQuery();
            return cmd;
        }

        public static object singleQuery(MySqlConnection mysql, string sql)
        {

            MySqlCommand cmd = new MySqlCommand(sql, mysql);
            var dt = new DataTable();
            dt.Load(cmd.ExecuteReader());
            var rows = dt.AsDataView();
            return rows;
        }

        public static int rowCount(DataView l)
        {
            return l.Count;
        }

        public static Client getPlayerFromName(string name)
        {
            Client target = null;
            lock (Program.ServerInstance.Clients) target = Program.ServerInstance.Clients.FirstOrDefault(c => c.DisplayName.ToLower().StartsWith(name.ToLower()));
            return target;
        }

        public static void addEventHandler(string eventName, object target, CallbackFunction callback)
        {

            if (eventName == "onResourceStart")
            {
                callback.Invoke("OK");
            }
            else if (eventName == "onPlayerConnect")
            {
                OnPlayerConnectEvents.Add(new Data(target, callback));
            }
            else if (eventName == "onPlayerDisconnect")
            {
                OnPlayerDisconnectEvents.Add(new Data(target, callback));
            }
            else if (eventName == "onPlayerKilled")
            {
                OnPlayerKilledEvents.Add(new Data(target, callback));
            }
            else if (eventName == "onChatMessage")
            {
                OnChatMessageEvents.Add(new Data(target, callback));
            }
            else if (eventName == "onConnectionRefused")
            {
                OnConnectionRefusedEvents.Add(new Data(target, callback));
            }
        }
    }
}