﻿using NLua;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Windows.Forms;
using Bend.Util;

namespace GTAServer
{
    public partial class main : Form
    {
        public static StreamWriter logFile;
        public static List<string> logList = new List<string>();
        public static List<string> playerList = new List<string>();
        public static bool started = false;
        public static int syncInterval = 200;
        
        public main()
        {
            InitializeComponent();
        }

        public class MyHttpServer : HttpServer
        {
            public MyHttpServer(int port)
                : base(port)
            {
            }

            public override void handleGETRequest(HttpProcessor p)
            {
                string file = p.http_url.ToString().Substring(1);

                if (file.StartsWith("resource_html_cache/") && File.Exists(file))
                {
                    StreamReader sr = new StreamReader(file);
                    string text = "";
                    while (sr.Peek() > -1)
                        text += sr.ReadLine() + Environment.NewLine;
                    sr.Close();
                    p.writeSuccess("text/plain");
                    p.outputStream.WriteLine(text);
                }
                else
                {
                    p.writeSuccess();
                    p.outputStream.WriteLine("<html><body><h1>Access denied!</h1><p>URL: " + file + "</p></body</html>");
                }
            }

            public override void handlePOSTRequest(HttpProcessor p, StreamReader inputData)
            {
                Console.WriteLine("POST request: {0}", p.http_url);
            }
        }

        private void main_Load(object sender, EventArgs e)
        {
            DateTime thisDay = DateTime.Today;
            if (!Directory.Exists("logs"))
                Directory.CreateDirectory("logs");
            logFile = new StreamWriter("logs/" + thisDay.ToString("d") + "log");

            ThreadPool.QueueUserWorkItem(p => startServer());

            timer1.Tick += delegate
            {
                if (logList.Count > 0)
                {
                    try
                    {
                        foreach (string s in logList)
                            richTextBox1.Text += s + Environment.NewLine;
                        logList = new List<string>();
                        richTextBox1.SelectionStart = richTextBox1.Text.Length;
                        richTextBox1.ScrollToCaret();
                    }
                    catch
                    {

                    }
                }
                if (started)
                {
                    label1.Text = "Játékosok: " + playerList.Count + "/" + Program.ServerInstance.MaxPlayers;
                }
                else
                {
                    label1.Text = "Indítás..";
                }
                if (playerList.Count > 0)
                {
                    try
                    {
                        listBox2.Items.Clear();
                        foreach (string s in playerList)
                            listBox2.Items.Add(s);
                    }
                    catch
                    {

                    }
                }
            };
            timer1.Start();
        }

        public static void addLog(string msg)
        {
            logList.Add(msg);
            logFile.WriteLine(msg);
            logFile.Flush();
        }

        public static void addPlayer(string name)
        {
            playerList.Add(name);
        }

        public static void removePlayer(string name)
        {
            playerList.Remove(name);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string cmd = textBox1.Text;
            string[] args = cmd.Split(' ');
            cmd = args[0];
            textBox1.Text = "";

            if (cmd.StartsWith("stop"))
            {
                Application.Exit();
            }
            else if(cmd.StartsWith("say"))
            {
                string ki = "";
                for (int i = 1; i < args.Length; i++)
                    ki += args[i] + " ";
                ServerScript.outputConsole("CONSOLE: " + ki);
                ServerScript.outputChatBox(ki, null, "CONSOLE");
            }
            else if (cmd.StartsWith("kick"))
            {
                if (args.Length < 2)
                {
                    ServerScript.outputConsole("kick [játékos] [indok]");
                }
                else
                {
                    string name = args[1];
                    string reason = "";
                    for (int i = 2; i < args.Length; i++)
                        reason += args[i] + " ";

                    Client player = ServerScript.getPlayerFromName(name);
                    if (player == null)
                    {
                        ServerScript.outputConsole(name + " nem található!");
                    }
                    else
                    {
                        player.NetConnection.Disconnect("Kidobás: " + reason);
                    }
                }
            }
            else
            {
                ServerScript.outputConsole("A parancs nem található: " + cmd);
            }
        }

        public static string generateRandomString()
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var stringChars = new char[8];
            var random = new Random();
            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            return new String(stringChars);
        }

        public static void startServer()
        {
            var settings = Program.ReadSettings(Program.Location + "config.xml");

            ServerScript.outputConsole("Server name: " + settings.Name);
            ServerScript.outputConsole("Port: " + settings.Port);
            ServerScript.outputConsole("HTTPPort: " + settings.HTTPPort);
            ServerScript.outputConsole("Maximum players: " + settings.MaxPlayers);
            ServerScript.outputConsole("Starting...");
            
            ServerScript sc = new ServerScript();

            Program.ServerInstance = new GameServer(settings.Port, settings.Name, settings.Gamemode);

            Lua lua = Program.lua;
            
            lua.DoString("root = '" + generateRandomString() + "'");

            lua.RegisterFunction("addEventHandler", sc, sc.GetType().GetMethod("addEventHandler"));

            //Chat Functions
            lua.RegisterFunction("outputConsole", sc, sc.GetType().GetMethod("outputConsole"));
            lua.RegisterFunction("outputChatBox", sc, sc.GetType().GetMethod("outputChatBox"));

            //Player Functions
            lua.RegisterFunction("getPlayerName", sc, sc.GetType().GetMethod("getPlayerName"));

            //Mysql Functions
            lua.RegisterFunction("connectMysql", sc, sc.GetType().GetMethod("connectMysql"));
            lua.RegisterFunction("executeQuery", sc, sc.GetType().GetMethod("executeQuery"));
            lua.RegisterFunction("singleQuery", sc, sc.GetType().GetMethod("singleQuery"));
            lua.RegisterFunction("rowCount", sc, sc.GetType().GetMethod("rowCount"));

            Program.ServerInstance.PasswordProtected = settings.PasswordProtected;
            Program.ServerInstance.Password = settings.Password;
            Program.ServerInstance.AnnounceSelf = settings.Announce;
            Program.ServerInstance.MasterServer = settings.MasterServer;
            Program.ServerInstance.MaxPlayers = settings.MaxPlayers;
            Program.ServerInstance.AllowDisplayNames = settings.AllowDisplayNames;

            if (settings.syncInterval < 50)
            {
                settings.syncInterval = 50;
                ServerScript.outputConsole("Minimum sync: 50ms");
            }
            else if (settings.syncInterval > 500) {
                settings.syncInterval = 500;
                ServerScript.outputConsole("Maximum sync: 500ms");
            }

            syncInterval = settings.syncInterval;

            Program.ServerInstance.Start(settings.Filterscripts);

            ServerScript.outputConsole("A szerver elindult.");

            started = true;

            Thread serverThread = new Thread(() =>
            {
                while (true)
                {
                    Thread.Sleep(syncInterval);
                    Program.ServerInstance.Tick();
                }
            });
            serverThread.Start();

            ServerScript.outputConsole("HTTP Server indítása...");
            HttpServer httpServer = new MyHttpServer(settings.HTTPPort);
            Thread httpServerThread = new Thread(new ThreadStart(httpServer.listen));
            httpServerThread.Start();
            ServerScript.outputConsole("HTTP Server elindult!");
        }
    }
}
